#include "Prim.h"





Prim::Prim(graphList * graph): listGraph(graph), tableGraph(nullptr)
{
}

Prim::Prim(graphTable * graph): tableGraph(graph), listGraph(nullptr)
{
}

graphList * Prim::listPrim()
{
	if (listGraph == nullptr) return false;
	int n, m, v;
	n = listGraph->getN();
	m = listGraph->getM();
	v = 0; //wierzcholek startowy 
	Edge edge;
	graphList* tree = new graphList(n);
	Queue* queue = new Queue(m); //kolejka piorytetowa krawedzi
	bool* visited = new bool[n];  //odwiedzone wierzcholki

	for (int i = 0; i < n; i++)
	{
		visited[i] = false;
	}

	visited[v] = true;  //zaczynamy od wierzchołka 0

	
	for (int i = 1; i < n; i++)
	{
		for (ListElem* e = listGraph->getVertices()[v]; e; e = e->next)  //przegladanie listy sasiadow 
		{
			if (!visited[e->n]) //jeżeli wierzchołek nie jest odwiedzony
			{
				edge.v1 = v; 
				edge.v2 = e->n;
				edge.weight = e->v;
				queue->push(edge);  //dodaje krawedz do kolejki
			}
		}

		do
		{
			edge = queue->getFront();
			queue->pop();

		} while (visited[edge.v2]); //dopoki nie znajde krawedzi do nowego wierzcholka

		tree->pushEdge(edge.v1, edge.v2, edge.weight);  //dodaj krawedz do drzewa
		tree->pushEdge(edge.v2, edge.v1, edge.weight);
		visited[edge.v2] = true; //oznacz wierzcholek jako odwiedzony
		v = edge.v2;
	}

	delete[] visited;
	delete queue;
	return tree;

}

graphTable* Prim::tablePrim()
{
	if (tableGraph == nullptr) return false;
	int n, m, v;
	n = tableGraph->getVertices();
	m = tableGraph->getEdge();
	v = 0; //wierzcholek startowy 
	Edge edge;
	graphTable* tree = new graphTable(n,n-1);
	Queue* queue = new Queue(m); //kolejka piorytetowa krawedzi
	bool* visited = new bool[n];  //odwiedzone wierzcholki

	for (int i = 0; i < n; i++)
	{
		visited[i] = false;
	}

	visited[0] = true;

	v = 0;

	for (int i = 0; i < n-1; i++)  // bedzie n-1 krawedzi
	{
		for (int j = 0; j < m; j++)
		{
			if (tableGraph->getGraph()[v][j] == 1)
			{
				for (int k = 0; k < n; k++)
				{
					if (tableGraph->getGraph()[k][j] == 1 && k != v)
					{
						edge.v1 = v;
						edge.v2 = k;
						edge.weight = tableGraph->getValue()[j];
						queue->push(edge);
						break;
					}
				}
				
			}
		}
		do
		{
			edge = queue->getFront();
			queue->pop();
		} while ( visited[edge.v2]);
		tree->addEdge(edge.v1, edge.v2, i, edge.weight, false);
		visited[edge.v2] = true;
		v = edge.v2;
	}
	delete[] visited;
	delete queue;
	return tree;
}

Prim::~Prim()
{
}
