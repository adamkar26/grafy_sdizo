#pragma once
#include"graphList.h"
#include"graphTable.h"
#include"BellmanFord.h"
#include"Dijkstra.h"
#include"Kruskal.h"
#include"Prim.h"
#include"Test.h"

class User
{
	
private:
	graphList * listgraph=nullptr;
	graphTable* tablegraph=nullptr;

public:
	User();
	void generate();
	void generateGraph(int v, int d,bool direct);
	void direct();
	void printGraph();
	void aBellmanFord();
	void aDijkstra();
	void inDirect();
	void readMenu();
	void readGraph(std::string path, bool direct);
	~User();
};

