#pragma once
#include "Queue.h"
#include"graphList.h"
#include"graphTable.h"
#include "ListElem.h"
class Prim
{
private:
	graphList * listGraph;
	graphTable* tableGraph;
public:
	Prim(graphList* graph);
	Prim(graphTable* graph);
	graphList* listPrim();
	graphTable* tablePrim();
	~Prim();
};

