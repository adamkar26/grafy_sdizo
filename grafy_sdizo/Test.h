#pragma once
#include "graphTable.h"
#include "graphList.h"
#include "BellmanFord.h"
#include "Dijkstra.h"
#include "Kruskal.h"
#include "Prim.h"
#include<chrono>
#include<fstream>
#include<iostream>
#include<iomanip>
class Test 
{

private:
	graphList* listgraph = nullptr;
	graphTable* tablegraph = nullptr;
	int n[5] = { 200,400,600,800,1000 };
	int p[4] = { 25,50,75,99 };
	double listResultBF[5][4];
	double tableResultBF[5][4];
	double listResultD[5][4];
	double tableResultD[5][4];
	double listResultP[5][4];
	double tableResultP[5][4];
	double listResultK[5][4];
	double tableResultK[5][4];
public:
	Test();
	 void makeTest();
	 void generateGraph(int v, int d, bool direct);
	~Test();
};

