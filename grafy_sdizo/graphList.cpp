#include "graphList.h"
#include <iostream>


graphList::graphList(int n)
{
	vertices = new ListElem*[n];
	m = 0;
	this->n = n;
	for (int i = 0; i < n; i++) vertices[i] = nullptr;
}


graphList::~graphList()
{
	for (int i = 0; i < n; i++)
	{
		ListElem* elem = vertices[i];
		ListElem* e;
		while (elem!= nullptr )
		{
			e = elem;
			elem = elem->next;
			delete e;
		}
	}
	delete[] vertices;
}

void graphList::print()
{
	for (int i = 0; i < n; i++)
	{
		ListElem * elem= vertices[i];
		std::cout << i << ": ";
		while (elem != nullptr)
		{
			elem->print();
			elem = elem->next;
		}
		std::cout << std::endl;
	}
}

void graphList::pushEdge(int start, int end, int v)
{
	ListElem* elem = vertices[start]; 
	ListElem* edge=new ListElem();
	edge->n = end;
	edge->next = elem;
	edge->v = v;
	vertices[start] = edge;
	m++;
}

int graphList::getN()
{
	return n;
}

int graphList::getM()
{
	return m;
}

ListElem ** graphList::getVertices()
{
	return vertices;
}

bool graphList::isEdge(int start, int end)
{
	ListElem* pv = vertices[start];
	if (pv != nullptr)
	{
		for (pv; pv; pv=pv->next)
		{
			if (pv->n == end) return true;
		}
	}
	return false;
}


