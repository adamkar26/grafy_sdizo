#include "DSStree.h"



DSStree::DSStree(int n)
{
	st = new DSSnode[n];
}


DSStree::~DSStree()
{
	delete[] st;
}

void DSStree::makeSet(int v)
{
	st[v].up = v;   //tworze nowy zbior- numer zbioru jest jego reprezentantem
	st[v].weight = 0;  //waga zero

}

int DSStree::findSet(int v)
{
	if (st[v].up != v) st[v].up = findSet(st[v].up);
	return st[v].up;
}

void DSStree::unionSetes(Edge e)
{
	int ru, rv;

	ru = findSet(e.v1);  //korze� z v1
	rv = findSet(e.v2);  //korze� z v2
	if (ru != rv)    //jezeli nie sa w jednym zbiorze
	{
		if (st[ru].weight > st[rv].weight) st[rv].up = ru;
		else
		{
			st[ru].up = rv;
			if (st[ru].weight == st[rv].weight) st[rv].weight++;
		}
	}
}
