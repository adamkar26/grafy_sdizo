#include "Stack.h"



Stack::Stack()
{
	S = nullptr;
}


Stack::~Stack()
{
	while (S) pop();
	
}

bool Stack::empty()
{
	return !S;
}

int Stack::top()
{
	return S->v;
}

void Stack::push(int v)
{
	SlistElem* e = new SlistElem;
	e->v = v;
	e->next = S;
	S = e;
}

void Stack::pop()
{
	if (S)
	{
		SlistElem* e = S;
		S = S->next;
		delete e;
	}
}
