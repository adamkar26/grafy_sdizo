#include "Queue.h"




Queue::Queue(int n)
{
	heap = new Edge[n];  // alokacja pamieci
	endHeap = 0;
}

Queue::~Queue()
{
	delete[] heap;
}

Edge Queue::getFront()
{
	return  heap[0];
	
}


void Queue::push(Edge e)
{
	int i, j;
	i = endHeap; //i na koncu kopca
	j = (i - 1) / 2;  // rodzic
	endHeap++; //zwiekszam koniec kopca
	while (i != 0 && (heap[j].weight > e.weight))
	{
		heap[i] = heap[j];
		i = j;
		j = (i - 1) / 2;
	}
	heap[i] = e;
}

void Queue::pop()
{
	int i, j;
	Edge e;
	if (endHeap)
	{
		e = heap[--endHeap];
		i = 0;
		j = 1;
		while (j<endHeap)
		{
			if ((j + 1 < endHeap) && (heap[j + 1].weight < heap[j].weight)) j++;
			if (e.weight <= heap[j].weight) break;
			heap[i] = heap[j];
			i = j;
			j = 2*j+1;
		}
		heap[i] = e;

	}
	
}
