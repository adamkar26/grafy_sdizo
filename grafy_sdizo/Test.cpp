#include "Test.h"

using namespace std;

Test::Test()
{
	
}


 void Test::makeTest()
{
	/*
	 for (int i = 0; i < 5; i++)
	 {

		 for (int j = 0; j < 4; j++)
		 {
			 
			 double sumL = 0;
			 double sumT = 0;
			 for (int k = 0; k < 100; k++)
			 {
				 generateGraph(n[i], p[j], true);
				 BellmanFord* test1 = new BellmanFord(listgraph);
				 BellmanFord* test2 = new BellmanFord(tablegraph);
				 int v1 = rand() % n[i];
				 int v2 = rand() % n[i];
				 auto start = chrono::high_resolution_clock::now();
				 test1->BellmanFordList(v1, v2);
				 auto end = chrono::high_resolution_clock::now();
				 chrono::duration<double> diff = end - start; //obliczam czas trawania operacji
				 sumL = sumL + diff.count();
				 std::cout << i<<" : " << j << " : " << k << endl;
				 delete test1;
				 
				 start = chrono::high_resolution_clock::now();
				 test2->BellmanFordTable(v1, v2);
				 end = chrono::high_resolution_clock::now();
				 diff = end - start; //obliczam czas trawania operacji
				 sumT = sumT + diff.count();
				 
				 delete test2;
				 

			 }
			 tableResultBF[i][j] = sumT / n[i];
			 listResultBF[i][j] = sumL / n[i];
			
		 }
	 }
	 
	 for (int i = 0; i < 5; i++)
	 {
	
		 for (int j = 0; j < 4; j++)
		 {
			
			 double sumL = 0;
			 double sumT = 0;
			 for (int k = 0; k < 100; k++)
			 {
				 generateGraph(n[i], p[j], true);
				 Dijkstra* test1 = new Dijkstra(listgraph);
				 Dijkstra* test2 = new Dijkstra(tablegraph);
				 int v1 = rand() % n[i];
				 int v2 = rand() % n[i];
				 auto start = chrono::high_resolution_clock::now();
				 test1->listDijkstra(v1, v2);
				 auto end = chrono::high_resolution_clock::now();
				 chrono::duration<double> diff = end - start; //obliczam czas trawania operacji
				 sumL = sumL + diff.count();
				 std::cout << i << " : " << j << " : " << k << endl;
				 delete test1;
				 
				 start = chrono::high_resolution_clock::now();
				 test2->tableDijkstra(v1, v2);
				 end = chrono::high_resolution_clock::now();
				 diff = end - start; //obliczam czas trawania operacji
				 sumT = sumT + diff.count();
				 
				 delete test2;
				 
			 }
			 tableResultD[i][j] = sumT / n[i];
			 listResultD[i][j] = sumL / n[i];
			 
		 }
	 }
	 
	 for (int i = 0; i < 5; i++)
	 {
		
		 for (int j = 0; j < 4; j++)
		 {
			
			 double sumL = 0;
			 double sumT = 0;
			 for (int k = 0; k < 100; k++)
			 {
				 generateGraph(n[i], p[j], false);
				 Prim* test1 = new Prim(listgraph);
				 Prim* test2 = new Prim(tablegraph);
				 auto start = chrono::high_resolution_clock::now();
				 graphList* t = test1->listPrim();
				 auto end = chrono::high_resolution_clock::now();
				 chrono::duration<double> diff = end - start; //obliczam czas trawania operacji
				 delete t;
				 sumL = sumL + diff.count();
				 std::cout << i << " : " << j << " : " << k << endl;
				 delete test1;
				 
				 start = chrono::high_resolution_clock::now();
				 graphTable* t1 = test2->tablePrim();
				 end = chrono::high_resolution_clock::now();
				 diff = end - start; //obliczam czas trawania operacji
				 delete t1;
				 sumT = sumT + diff.count();
				 
				 delete test2;
				 
			 }
			 tableResultP[i][j] = sumT / n[i];
			 listResultP[i][j] = sumL / n[i];
			
		 }
	 }
	 */
	 for (int i = 0; i < 5; i++)
	 {
		
		 for (int j = 0; j < 4; j++)
		 {
			
			 double sumL = 0;
			 double sumT = 0;
			 for (int k = 0; k < 100; k++)
			 {
				 generateGraph(n[i], p[j], false);
				 Kruskal* test1 = new Kruskal(listgraph);
				 Kruskal* test2 = new Kruskal(tablegraph);
				 auto start = chrono::high_resolution_clock::now();
				 graphList* t = test1->listKurskal();
				 auto end = chrono::high_resolution_clock::now();
				 chrono::duration<double> diff = end - start; //obliczam czas trawania operacji
				 delete t;
				 sumL = sumL + diff.count();
				 std::cout << i << " : " << j << " : " << k << endl;
				 delete test1;
				 
				 start = chrono::high_resolution_clock::now();
				 graphTable* t1 = test2->tableKruskal();
				 end = chrono::high_resolution_clock::now();
				 diff = end - start; //obliczam czas trawania operacji
				 delete t1;
				 sumT = sumT + diff.count();
				
				 delete test2;
				 
			 }
			 tableResultK[i][j] = sumT / n[i];
			 listResultK[i][j] = sumL / n[i];
			

		 }
	 }

	 fstream plik1;
	 fstream plik2;
	 fstream plik3;
	 fstream plik4;
	 fstream plik5;
	 fstream plik6;
	 fstream plik7;
	 fstream plik8;


	 plik1.open("LBellmanFord.csv", ios::out);
	 plik2.open("LDijkstra.csv", ios::out);
	 plik3.open("LPrim.csv", ios::out);
	 plik4.open("LKruskal.csv", ios::out);
	 plik5.open("TBellmanFord.csv", ios::out);
	 plik6.open("TDijkstra.csv", ios::out);
	 plik7.open("TPrim.csv", ios::out);
	 plik8.open("TKruskal.csv", ios::out);

	 for (int i = 0; i < 5; i++)
	 {
		 for (int j = 0; j < 4; j++)
		 {
			 plik1 << setprecision(4)<<listResultBF[i][j]<<",";
			 plik2 << setprecision(4)<<listResultD[i][j]<< ",";
			 plik3 << setprecision(4)<< listResultP[i][j] << ",";
			 plik4 << setprecision(4)<< listResultK[i][j] << ",";
			 plik5 << setprecision(4)<<tableResultBF[i][j] << ",";
			 plik6 << setprecision(4)<<tableResultD[i][j] << ",";
			 plik7 << setprecision(4)<<tableResultP[i][j] << ",";
			 plik8 << setprecision(4)<<tableResultK[i][j] << ",";
		 }
		 plik1 << endl;
		 plik2 << endl;
		 plik3 << endl;
		 plik4 << endl;
		 plik5 << endl;
		 plik6 << endl;
		 plik7 << endl;
		 plik8 << endl;
	 }
	 plik1.close();
	 plik2.close();
	 plik3.close();
	 plik4.close();
	 plik5.close();
	 plik6.close();
	 plik7.close();
	 plik8.close();
}

void Test::generateGraph(int v, int d, bool direct)
{
	int m = 0;  //liczba krawedzi
	if (!direct) m = (v*(v - 1)*d) / 200;
	else  m = (v*(v - 1)*d) / 100;

	if (listgraph != nullptr) delete listgraph;
	if (tablegraph != nullptr) delete tablegraph;
	tablegraph = new graphTable(v, m);
	listgraph = new graphList(v);
	for (int i = 0; i < m; i++)
	{
		int start, end;
		do
		{
			start = rand() % v; //wierzchołek startowy
			end = rand() % v;  //wierzcholek koncowy
			while (end == start) end = rand() % v; //jezeli petla
		} while (listgraph->isEdge(start, end));  //czy krawedz juz istnieje?
		int value = rand() % 100 + 1; //waga od 1 do 100
		listgraph->pushEdge(start, end, value);
		if (!direct) listgraph->pushEdge(end, start, value);
		tablegraph->addEdge(start, end, i, value, direct);
	}
	if (!tablegraph->isConnected())  //jezeli wygenerwoay graf nie jest spojny
	{
		delete tablegraph;
		delete listgraph;
		generateGraph(v, d, direct);
	}

}


Test::~Test()
{
}
