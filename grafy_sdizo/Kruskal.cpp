#include "Kruskal.h"



Kruskal::~Kruskal()
{
}

graphList * Kruskal::listKurskal()
{
	int n = listGraph->getN();
	int m = listGraph->getM();
	Edge e;
	if (listGraph == nullptr) return nullptr;
	graphList* graph = new graphList(n);
	DSStree* tree = new DSStree(n);
	Queue* queue = new Queue(m);


	for (int i = 0; i <n; i++)
	{
		tree->makeSet(i);  //tworzy zbiory rozlaczne dla kazdego wierzcholka
	}

	
	for (int j = 0; j < n; j++)
	{
		ListElem* vertices = listGraph->getVertices()[j];
		while (vertices != nullptr)
		{
			e.v1 = j;
			e.v2 = vertices->n;
			e.weight = (vertices->v);
			vertices = vertices->next;
			queue->push(e);

		}
		
	}
	
	for (int i = 1; i < n; i++)
	{
		do
		{
			e = queue->getFront();  //pobierz krawedz
			queue->pop();//usun z kopca
			queue->pop(); //usun blizniaka
		} while (tree->findSet(e.v1) == tree->findSet(e.v2));//tak d�ugo dop�ki wierzcho�ki le�a w tym samym zbiorze
		graph->pushEdge(e.v1, e.v2, e.weight);
		graph->pushEdge(e.v2, e.v1, e.weight);  //dodaje krawedz w obu kierunkach
		tree->unionSetes(e); //�acze zbiory
	}


	delete tree;
	delete queue;
	return graph;
}

graphTable * Kruskal::tableKruskal()
{
	int n = tableGraph->getVertices(); //ilosc wierzcholkow
	int m = tableGraph->getEdge(); //ilosc krawedzi
	Edge e;
	if (tableGraph == nullptr) return nullptr;
	graphTable* graph = new graphTable(n,n-1);
	DSStree* tree = new DSStree(n);
	Queue* queue = new Queue(m);

	for (int i = 0; i <n; i++)
	{
		tree->makeSet(i);  //tworzy zbiory rozlaczne dla kazdego wierzcholka
	}


	for (int i = 0; i < m; i++)
	{
		bool first = true;
		for (int j = 0; j < n; j++)
		{
			if (tableGraph->getGraph()[j][i] == 1)
			{
				if (first)
				{
					first = false;
					e.v1 = j;
				}
				else
				{
					e.v2 = j;
					e.weight = tableGraph->getValue()[i];
					first = true;
					queue->push(e);
					break;
				}

				
			}
		}


	}

	for (int i = 0; i < n-1; i++)
	{
		do
		{
			e = queue->getFront();  //pobierz krawedz
			queue->pop();//usun z kopca

		} while (tree->findSet(e.v1) == tree->findSet(e.v2));  //tak d�ugo dop�ki kraw�dzie le�a w tym samym zbiorze
		graph->addEdge(e.v1, e.v2, i, e.weight, false);  //dodaje krawedz w obu kierunkach
		tree->unionSetes(e); //�acze zbiory
	}


	delete tree;
	delete queue;
	return graph;
}


Kruskal::Kruskal(graphList * graph)
{
	listGraph = graph;
	tableGraph = nullptr;
}

Kruskal::Kruskal(graphTable * graph)
{
	tableGraph = graph;
	listGraph = nullptr;
}

