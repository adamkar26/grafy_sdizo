#pragma once
#include "ListElem.h"
class graphList
{
	private:
		int n; //ilosc wierzcholkow
		int m; //liczba krawedzi (skierowanych)
		ListElem** vertices;

public:
	graphList(int n);
	~graphList();
	void print();
	void pushEdge(int start,int end, int v); //dodaje krawedz
	int getN();
	int getM();
	ListElem** getVertices();
	bool isEdge(int start, int end); //czy istnieje krawedz laczaca wierzcholki
};

