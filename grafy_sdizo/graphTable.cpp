#include "graphTable.h"
#include <iostream>
#include<iomanip>
using namespace std;


graphTable::graphTable(int v, int edge)
{
	vertices = v;
	this->edge = edge;
	graph = new int*[v];
	for (int i = 0; i < v; i++)
	{
		graph[i] = new int[edge];
		for (int j = 0; j < edge; j++) graph[i][j] = 0;
	}
	value = new int[edge];

}


graphTable::~graphTable()
{
	for (int i = 0; i < vertices; i++) delete[] graph[i];
	delete[] graph;
	delete[] value;
}

void graphTable::addEdge(int start,int stop, int edge, int val, bool direct)
{
	graph[start][edge] = 1;
	if(direct) graph[stop][edge] = -1;
	else graph[stop][edge] = 1;
	value[edge] = val;
}

void graphTable::print()
{
	cout << "   ";
	for (int i = 0; i < edge; i++) cout <<setw(2)<< i << " ";
	cout << endl;
	for (int i = 0; i < vertices; i++)
	{
		cout << i << ": ";
		for (int j = 0; j < edge; j++) cout <<setw(2)<< graph[i][j] << " ";
		cout << endl;
	}
	cout << endl;
	cout << " wagi krawedzi: " << endl;
	for (int i = 0; i < edge; i++) cout << i << ":" << value[i] << "  ";
	cout << endl;
}

int graphTable::getVertices()
{
	return vertices;
}

int graphTable::getEdge()
{
	return edge;
}

int ** graphTable::getGraph()
{
	return graph;
}

int * graphTable::getValue()
{
	return value;
}

bool graphTable::isEdge(int start, int end,bool direct)
{
	for (int i = 0; i < edge; i++)
	{
		if (graph[start][i] == 1)
		{
			if (!direct && graph[end][i] == 1) return true;
			else if (direct && graph[end][i] == -1) return true;
		}
	}
	return false;
}

bool graphTable::isConnected()
{
	int counter = 0; //licznik wierzcholkow
	Stack* S = new Stack();
	bool* visited = new bool[vertices]; 
	S->push(0); // zero na stos
	visited[0] = true;
	int v; 
	for (int i = 1; i < vertices; i++) visited[i] = false;
	while (!S->empty())
	{
		v = S->top();  //pobieramy szczyt stosu
		S->pop();
		counter++;
		
		for (int j = 0; j < edge; j++)
		{
			if (graph[v][j] == 1)
			{
				for (int k = 0; k < vertices; k++)
				{
					if (graph[k][j] == -1 || (graph[k][j] == 1 && k != v))
					{
						if (!visited[k])
						{
							visited[k] = true;
							S->push(k);
						}
						break;
					}
				}
			}
		}
		
	}

	delete S;
	if (counter == vertices) return true;
	else return false;
}
