#pragma once
#include"Queue.h"
struct DSSnode
{
	int up, weight;
};

class DSStree
{
private:
	DSSnode * st;   //tablica zbior�w roz��cznych

public:
	DSStree(int n);
	~DSStree();
	void makeSet(int v); //tworzy zbi�r 1-elementowy
	int findSet(int v); //zwraca indeks ojca zbioru do ktr�rego nale�y v
	void unionSetes(Edge e); //��czy dwa zbiory

};

