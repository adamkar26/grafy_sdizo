#pragma once
#include"Stack.h"
class graphTable
{
private:
	int** graph;
	int* value;
	int vertices;
	int edge;
	
public:
	graphTable(int v, int edge);
	~graphTable();
	void addEdge(int start, int stop, int edge, int val, bool direct);
	void print();
	int getVertices();
	int getEdge();
	int** getGraph();
	int* getValue();
	bool isEdge(int start, int end, bool direct);
	bool isConnected();

};

