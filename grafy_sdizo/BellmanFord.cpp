#include "BellmanFord.h"
#include<iostream>


BellmanFord::BellmanFord(graphList * graph): listgraph(graph), tablegraph(nullptr)
{
}

BellmanFord::BellmanFord(graphTable * graph): tablegraph(graph), listgraph(nullptr)
{
}

BellmanFord::~BellmanFord()
{
}

std::string BellmanFord::BellmanFordList(int v1, int v2)
{
	if (listgraph == nullptr) return "";
	ListElem** graph = listgraph->getVertices(); //tablica list sasiedztwa
	int n = listgraph->getN(); //liczba wierzcholkow
	int m = listgraph->getM(); //liczba krawedzi
	int* d = new int[n];  //tablica kosztow dojscia
	int* p = new int[n]; //tablica poprzedników

	for (int i = 0; i < n; i++)  //inicjalizacja danych
	{
		d[i] = MAXINT;
		p[i] = -1;
	}

	
	bool test; //czy trzeba sprawdzic obecnosc ujemnego cyklu
	d[v1] = 0; //koszt dojscia do startu

	for (int i = 1; i < n; i++)  //relakscaja w petli
	{
		test = true;
		for (int j = 0; j < n; j++) //przejscie przez kolejne wierzcholki grafu
		{
			for (ListElem* pv = graph[j]; pv; pv = pv->next)  //przegladam liste sasiadow
			{
				if (d[pv->n] > d[j] + pv->v) //warunwk relaksacji
				{
					test = false;   //zmiana w d i p
					d[pv->n] = d[j] + pv->v;
					p[pv->n] = j;
				}
			}
		}
		if (test) break;  //jezeli nie bylo zmian-koniec
	}

	if (!test) //jezeli byly zmiany
	{
		for (int i = 0; i < n; i++)
		{
			for (ListElem* pv = graph[i]; pv; pv = pv->next)
				if (d[pv->n] > d[i] + pv->v) return "graf zawiera cykl ujmeny";
		}
	}

	int* s = new int[n];  //stos
	int sptr = 0; //wskaznik szczytu stosu 



	std::string str = std::to_string(v2) + ": ";
	for (int i = v2; i != -1; i = p[i]) //dodanie na stos
		s[sptr++] = i;

	//sciagnie ze stosu
	while (sptr)
		str = str + std::to_string(s[--sptr]) + " ";
	str = str + " $" + std::to_string(d[v2]);

	delete[] s;
	delete[] d;
	delete[] p;
	return str;

}

std::string BellmanFord::BellmanFordTable(int v1, int v2)
{
	if (tablegraph == nullptr) return "";
	int** graph = tablegraph->getGraph(); //miacierz
	int n =tablegraph->getVertices(); //liczba wierzcholkow
	int m = tablegraph->getEdge(); //liczba krawedzi
	int* v = tablegraph->getValue();  //tablica wartosci
	int* d = new int[n];  //tablica kosztow dojscia
	int* p = new int[n]; //tablica poprzedników

	for (int i = 0; i < n; i++)  //inicjalizacja danych
	{
		d[i] = MAXINT;
		p[i] = -1;
	}


	bool test; //czy trzeba sprawdzic obecnosc ujemnego cyklu
	d[v1] = 0; //koszt dojscia do startu

	for (int i = 1; i < n; i++)  //relakscaja w petli
	{
		test = true;
		for (int j = 0; j < n; j++) //przejscie przez kolejne wierzcholki grafu
		{
			for (int k = 0; k < m;k++)  //szukam krawedzi
			{
				if (graph[j][k] == 1)
				{
					int stop; //koniec krawedzi
					for(int l=0;l<n;l++)
						if (graph[l][k] == -1)
						{
							stop = l;
							break;
						}
					if (d[stop] > d[j] + v[k]) //warunwk relaksacji
					{
						test = false;   //zmiana w d i p
						d[stop] = d[j] +v[k];
						p[stop] = j;
					}
				}
			}
		}
	}

	if (!test) //jezeli byly zmiany
	{
		int k;
		for (int i = 0; i < m; i++)
		{
			for (int j = 0; j < n; j++)
			{
				if (graph[j][i] == 1)
				{
					k = j;
					continue;
				}
				if (graph[j][i] == -1)
				{
					if (d[j] > d[k] + v[i]) return "";
					break;
				}
				
			}
		}
				
			
		
	}

	int* s = new int[n];  //stos
	int sptr = 0; //wskaznik szczytu stosu 
	
	std::string str = std::to_string(v2) + ": ";
	for (int i = v2; i != -1; i = p[i]) //dodanie na stos
		s[sptr++] = i;

	//sciagnie ze stosu
	while (sptr)
		str = str + std::to_string(s[--sptr]) + " ";
	str = str + " $" + std::to_string(d[v2]);
	delete[] s;
	delete[] d;
	delete[] p;

	return str;
}


