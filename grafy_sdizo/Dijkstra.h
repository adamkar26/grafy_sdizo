#pragma once
#include "graphList.h"
#include "graphTable.h"
#include<string>
class Dijkstra
{
private: 
	graphList * listgraph;
	graphTable* tablegraph;
	const int MAXINT = 2147483647;  // imiitacja nieskonczonosci
public:
	Dijkstra(graphList* graph);
	Dijkstra(graphTable* graph);
	~Dijkstra();
	std::string listDijkstra(int v1, int v2);
	std::string tableDijkstra(int v1, int v2);
};

