#pragma once
#include "graphList.h"
#include "graphTable.h"
#include "DSStree.h"
class Kruskal
{
private:
	graphList* listGraph;
	graphTable* tableGraph;
public:
	Kruskal(graphList* graph);
	Kruskal(graphTable* graph);
	~Kruskal();
	graphList* listKurskal();
	graphTable* tableKruskal();
};

