#include "User.h"
#include<iostream>
#include<fstream>
using namespace std;


User::User()
{
	int choose=0;
	Test * test = new Test();
	while (true)
	{
		system("cls");
		cout << "----Menu-----" << endl;
		cout << "1. generuj graf" << endl;
		cout << "2. wczytaj graf z pliku" << endl;
		cout << "3. testy" << endl;
		cout << "4. zakoncz program" << endl;
		cin >> choose;
		switch (choose)
		{
		case 1:
			generate();
		case 2:
			readMenu();
			break;
		case 3:
			test->makeTest();
			break;
		case 4:
			return;
		default:
			cout << "Bledny wybor" << endl;
			break;
		}
	}
	delete test;
}

void  User::generate()
{
	system("cls");
	int choose = 0, v, d ;
	while (true)
	{
		system("cls");
		cout << "1. graf skierowany" << endl;
		cout << "2. graf nieskierowany" << endl;
		cout << "3. cofnij" << endl;
		cin >> choose;
		switch (choose)
		{
		case 1:
			cout << "Podaj liczbe wierzcho�k�w: ";
			cin >> v;
			cout << "Podaj gestosc grafu [%]: ";
			cin >> d;
			generateGraph(v, d, true);
	
			direct();
			break;
		case 2:
			cout << "Podaj liczbe wierzcho�k�w: ";
			cin >> v;
			cout << "Podaj gestosc grafu [%]: ";
			cin >> d;
			generateGraph(v, d, false);
		
			inDirect();
			break;
		case 3:
			return;
		default:
			cout << "Z�y wyb�r" << endl;
		}
	}
}

void User::generateGraph(int v, int d,bool direct)
{
	int m=0;  //liczba krawedzi
	if(!direct) m= (v*(v - 1)*d) / 200;
	else  m = (v*(v - 1)*d) / 100;
	
		if (listgraph != nullptr) delete listgraph;
		if (tablegraph != nullptr) delete tablegraph;
		tablegraph = new graphTable(v, m);
		listgraph = new graphList(v);
		for (int i = 0; i < m; i++)
		{
			int start, end;
			do
			{
				start = rand() % v; //wierzcho�ek startowy
				end = rand() % v;  //wierzcholek koncowy
				while (end == start) end = rand() % v; //jezeli petla
			} while (listgraph->isEdge(start, end));  //czy krawedz juz istnieje?
			int value = rand() % 100 + 1; //waga od 1 do 100
			listgraph->pushEdge(start, end, value);
			if(!direct) listgraph->pushEdge(end, start, value);
			tablegraph->addEdge(start, end, i, value, direct);
		}
		if (!tablegraph->isConnected())  //jezeli wygenerwoay graf nie jest spojny
		{
			delete tablegraph;
			delete listgraph;
			generateGraph(v, d, direct);
		}

}

void User::direct()
{
	int choose;
	while (true)
	{
		system("cls");
		cout << "1. wyswietl graf " << endl;
		cout << "2. algorytm Bellmana- Forda" << endl;
		cout << "3. algoyrtm Dijkstry" << endl;
		cout << "4. cofnij" << endl;
		cin >> choose;

		switch (choose)
		{
		case 1:
			printGraph();
			break;
		case 2:
			aBellmanFord();
			getchar();
			getchar();
			break;
		case 3:
			aDijkstra();
			getchar();
			getchar();
			break;
		case 4:
			return;
		default:
			cout << "B��dny wyb�r" << endl;
			break;
		}


	}
}

void User::printGraph()
{
	int choose=0;
	system("cls");
	while (true)
	{
		cout << "1. reprezentacja macierzowa" << endl;
		cout << "2. reprezentacja listowa " << endl;
		cout << "3. cofnij" << endl;
		cin >> choose;
		switch (choose)
		{
		case 1:
			tablegraph->print();
			getchar();
			break;
		case 2:
			listgraph->print();
			getchar();
			break;
		case 3:
			return;

		default:
			cout << "Z�y wyb�r" << endl;
		}
	}
}

void User::aBellmanFord()
{
	int start, end;
	
	system("cls");
	cout << "Podaj wierzcholek poczatkowy: ";
	cin >> start;
	cout << "Podaj wierzcholek koncowy: ";
	cin >> end;
	BellmanFord* test = new BellmanFord(listgraph);
	cout << "Wynik dla reprezentacji listowej: " << test->BellmanFordList(start, end) << endl;
	delete test;
	test = new BellmanFord(tablegraph);
	cout << "Wynik dla reprezentacji macierzowej: " << test->BellmanFordTable(start, end) << endl;
	delete test;

}

void User::aDijkstra()
{
	int start, end;

	system("cls");
	cout << "Podaj wierzcholek poczatkowy: ";
	cin >> start;
	cout << "Podaj wierzcholek koncowy: ";
	cin >> end;
	Dijkstra* test = new Dijkstra(listgraph);
	cout << "Wynik dla reprezentacji listowej: " << test->listDijkstra(start, end) << endl;
	delete test;
	test = new Dijkstra(tablegraph);
	cout << "Wynik dla reprezentacji macierzowej: " << test->tableDijkstra(start, end) << endl;
	delete test;
}

void User::inDirect()
{
	int choose;
	while (true)
	{
		system("cls");
		cout << "1. wyswietl graf " << endl;
		cout << "2. algorytm Kruskala" << endl;
		cout << "3. algoyrtm Prima" << endl;
		cout << "4. cofnij" << endl;
		cin >> choose;

		graphList* graph1;
		graphTable* graph2;
		Prim * test1;
		Kruskal * test;

		switch (choose)
		{
		case 1:
			printGraph();
			getchar();
			break;
		case 2:
			test = new Kruskal(listgraph);
			cout << "Wynik dla reprezentacji listowej " << endl;
		    graph1=test->listKurskal();
			graph1->print();
			delete graph1;
			delete test;
			test = new Kruskal(tablegraph);
			graph2 = test->tableKruskal();
			cout << "Wynik dla reprezentacji macierzowej  " << endl;
			graph2->print();
			delete graph2;
			delete test;
			getchar();
			getchar();
			break;
		case 3:
			test1 = new Prim(listgraph);
			cout << "Wynik dla reprezentacji listowej " << endl;
			graph1 = test1->listPrim();
			graph1->print();
			delete graph1;
			delete test1;
			test1 = new Prim(tablegraph);
			graph2 = test1->tablePrim();
			cout << "Wynik dla reprezentacji macierzowej  " << endl;
			graph2->print();
			getchar();
			getchar();
			delete graph2;
			delete test1;
			break;
			
			break;
		case 4:
			return;
		default:
			cout << "B��dny wyb�r" << endl;
			break;
		}


	}
}

void User::readMenu()
{
	string str;
	int choose;
	bool direct;
	while (true)
	{
	
	system("cls");
	cout << "Podaj nazwe pliku: ";
	cin >> str;
	cout << "Wybierz typ grafu: " << endl;
	cout << "1. skierowany (najkrotsza sciezka) " << endl;
	cout << "2. nieskierowany (minimalne drzewo rozpinajace) " << endl;
	cout << "3. cofnij" << endl;
	cin >> choose;
	switch (choose)
	{
	case 1: 
		readGraph(str, true);
		break;
	case 2:
		readGraph(str, false);
		break;
	case 3:
		return;
	default:
		cout << " Bledny wybor" << endl;
			break;
	}
}
}

void User::readGraph(string path, bool direct)
{
	fstream plik;
	plik.open(path);
	if (!plik.good())
	{
		cout << "Blad otwarcia pliku!" << endl;
		getchar();
		getchar();
		return;
	}
	int n, m;
	plik >> m >> n;  //liczba krawedzi, liczba wierzcholkow
	if (tablegraph != nullptr) delete tablegraph;
	if (listgraph != nullptr) delete listgraph;
	tablegraph = new graphTable(n, m);
	listgraph = new graphList(n);
	int v1, v2, weight;
	for(int i=0;i<m;i++)
	{
		plik >> v1 >> v2 >> weight;
		tablegraph->addEdge(v1, v2, i, weight, direct);
		listgraph->pushEdge(v1, v2, weight);
		if(!direct) listgraph->pushEdge(v2, v1, weight);
	}
	plik.close();
	if (!tablegraph->isConnected())
	{
		cout << "Wczytany graf nie jest spojny!" << endl;
		getchar();
		getchar();
		return;
	}
	if (!direct) inDirect();
	else this->direct();

}



User::~User()
{
	delete listgraph;
	delete tablegraph;
}
