#include "Dijkstra.h"


Dijkstra::Dijkstra(graphList * graph): listgraph(graph), tablegraph(nullptr)
{

}

Dijkstra::Dijkstra(graphTable * graph): tablegraph(graph), listgraph(nullptr)
{
}


Dijkstra::~Dijkstra()
{
}

std::string Dijkstra::listDijkstra(int v1, int v2)
{
	if (listgraph == nullptr) return "";
	
	ListElem** graf= listgraph->getVertices();  // tablica list sasiedztwa
	int n = listgraph->getN();  //liczba wierzcholkow
	int m = listgraph->getM(); //liczba krawedzi
	int* d = new int[n]; // tablica kosztow dojscia 
	int* p = new int[n]; //tablica poprzednikow
	int* s = new int[n]; //stos
	int* h = new int[n]; //kopiec
	int* hp = new int[n]; //pozycje w kopcu
	int sptr = 0; //wskaznik szczytu stosu 
	bool *QS=new bool[n]; // zbiory Q i S

	// inicjacja tablic
	for (int i = 0; i < n; i++)
	{
		d[i] = MAXINT;
		p[i] = -1;
		QS[i] = false; // wszystkie w zbiorze Q
		h[i] = hp[i] = i;

	}

	if (v1 >= n || v2 >= n) return "";

	d[v1] = 0;  //koszt dojscia do startu 0
	int x = h[0];
	h[0] = h[v1]; 
	h[v1] = x;
	hp[v1] = 0; 
	hp[0] = v1; //v1 na pierwszym miejscu kopca

	int min, parent, left, right, child;
	int hlen = n;  //wielkosc kopca
	for (int i = 0; i < n; i++)
	{
		min = h[0]; //korzen kopca- minimum
		// usuwam korzen i odtwarzam wlasnosc kopca
		h[0] = h[--hlen]; // ostatni elment na szczyt
		hp[h[0]] = parent = 0;

		while (true)
		{
			left = 2 * parent + 1;
			right = left + 1;
			if (left >= hlen) break; // jezeli brak potomkow koniec
			if (right < hlen && d[h[left]] > d[h[right]])
			{
				left = right;
			}
			if (d[h[parent]] <= d[h[left]]) break; // wlasnosc kopca zachowana- koniec
			//otworzenie wlasnosci kopca
			x = h[parent];
			h[parent] = h[left]; 
			h[left] = x;

			hp[h[parent]] = parent;
			hp[h[left]] = left;
			parent = left; 
		}

		QS[min] = true; //znaleziony wierzcholek przenosze do S

		ListElem* pw;
		for (pw = graf[min]; pw; pw = pw->next)
		{
			if (!QS[pw->n] && (d[pw->n] > d[min] + pw->v)) //jezeli mozna dokonac relaksacji
			{
				d[pw->n] = d[min] + pw->v;
				p[pw->n] = min;
			}
			// otworzenie wlasnosci kopca
			for (child = hp[pw->n]; child; child = parent)
			{
				parent = child / 2;
				if (d[h[parent]] <= d[h[child]]) break;
				x = h[parent]; h[parent] = h[child]; h[child] = x;
				hp[h[parent]] = parent; hp[h[child]] = child;
			}
		}
	}
	//zapis wyniku do stringu 
	
	std::string str = std::to_string(v2) + ": ";
	for (int i = v2; i > -1; i = p[i]) s[sptr++] = i;  //zapis na stos kolejnych wierzcholkow
	while (sptr) str = str +std::to_string(s[--sptr]) + " ";
	str = str + "$" + std::to_string(d[v2]);
	



	delete[] d;
	delete[] p;
	delete[] QS;
	delete[] s;
	delete[] h;
	delete[] hp;

	return str;
	
	
}

std::string Dijkstra::tableDijkstra(int v1, int v2)
{
	if (tablegraph == nullptr) return "";
	
	int** graph = tablegraph->getGraph();  // graf
	int n = tablegraph->getVertices();  //liczba wierzcholkow
	int m = tablegraph->getEdge(); //liczba krawedzi
	int* value = tablegraph->getValue(); //tablica wartosci krawedzi
	int* d = new int[n]; // tablica kosztow dojscia 
	int* p = new int[n]; //tablica poprzednikow
	int* s = new int[n]; //stos
	int* h = new int[n]; //kopiec
	int* hp = new int[n]; //pozycje w kopcu
	bool *QS = new bool[n]; // zbiory Q i S

	int sptr = 0; //wskaznik szczytu stosu 

				  // inicjacja tablic
	for (int i = 0; i < n; i++)
	{
		d[i] = MAXINT;
		p[i] = -1;
		QS[i] = false; // wszystkie w zbiorze Q
		h[i] = hp[i] = i;

	}

	if (v1 >= n || v2 >= n) return "";

	d[v1] = 0;  //koszt dojscia do startu 0
	int x = h[0];
	h[0] = h[v1];
	h[v1] = x;
	h[v1] = x;
	hp[v1] = 0;
	hp[0] = v1; //v1 na pierwszym miejscu kopca

	int min, parent, left, right, child;
	int hlen = n;  //wielkosc kopca
	for (int i = 0; i < n; i++)
	{
		min = h[0]; //korzen kopca- minimum
					// usuwam korzen i odtwarzam wlasnosc kopca
		h[0] = h[--hlen]; // ostatni elment na szczyt
		hp[h[0]] = parent = 0;

		while (true)
		{
			left = 2 * parent + 1;
			right = left + 1;
			if (left >= hlen) break; // jezeli brak potomkow koniec
			if (right < hlen && d[h[left]] > d[h[right]])
			{
				left = right;
			}
			if (d[h[parent]] <= d[h[left]]) break; // wlasnosc kopca zachowana- koniec
												   //otworzenie wlasnosci kopca
			x = h[parent];
			h[parent] = h[left];
			h[left] = x;

			hp[h[parent]] = parent;
			hp[h[left]] = left;
			parent = left;
		}

		QS[min] = true; //znaleziony wierzcholek przenosze do S

		int end;
		for (int j=0; j<m; j++)
		{
			if (graph[min][j] == 1)  //jezeli znalezlismy krawedz
			{
				for (int k = 0; k < n; k++)
				{
					if (graph[k][j] == -1)
					{
						end = k; //wierzcholek koncowy
						break;
					}
				}
				if (!QS[end] && (d[end] > d[min] + value[j])) //jezeli mozna dokonac relaksacji
				{
					d[end] = d[min] + value[j];
					p[end] = min;
				}
				// otworzenie wlasnosci kopca
				for (child = hp[end]; child; child = parent)
				{
					parent = child / 2;
					if (d[h[parent]] <= d[h[child]]) break;
					x = h[parent]; h[parent] = h[child]; h[child] = x;
					hp[h[parent]] = parent; hp[h[child]] = child;
				}
				
			}
			
		}
	}
	//zapis wyniku do stringu 
	
	std::string str = std::to_string(v2) + ": ";
	for (int i = v2; i > -1; i = p[i]) s[sptr++] = i;  //zapis na stos kolejnych wierzcholkow
	while (sptr) str = str + std::to_string(s[--sptr]) + " ";
	str = str + "$" + std::to_string(d[v2]);
	
	
	

	delete[] d;
	delete[] p;
	delete[] QS;
	delete[] s;
	delete[] h;
	delete[] hp;

	return str;
	
}
