#pragma once

struct Edge
{
	int v1, v2, weight;
};
// implementacja kolejki za pomoc� kopca
class Queue
{
private:
	

	Edge* heap; // kopiec
	int endHeap; //ostatnia pozycja (indeks kopca)

	
public:
	Queue(int n);
	~Queue();
	Edge  getFront();
	void push(Edge e);
	void pop();
};

