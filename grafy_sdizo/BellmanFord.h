#pragma once
#include"graphList.h"
#include"graphTable.h"
#include<string>

class BellmanFord
{
private:
	graphList* listgraph;
	graphTable* tablegraph;
	const int MAXINT = 2147000000;  // imiitacja nieskonczonosci
public:
	BellmanFord(graphList* graph);
	BellmanFord(graphTable* graph);
	~BellmanFord();
	std::string BellmanFordList(int v1, int v2);
	std::string BellmanFordTable(int v1, int v2);
};

