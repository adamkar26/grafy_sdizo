#pragma once
struct SlistElem
{
	SlistElem* next;
	int v;
};

class Stack
{
private:
	SlistElem * S; //lista przechowywyjaca stos
public:
	Stack();
	~Stack();
	bool empty();  //sprawdza czy pusty
	int top();    //zwraca szczyt
	void push(int v);  //umieszcza wierzcholek na stos
	void pop();  //usuwa ze szczytu
};

